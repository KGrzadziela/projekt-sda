<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Currency Rates Checker</title>
<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />

</head>

<body>
	<div class='container'>

		<form method="post" action="LoginServlet" class='well'>
			<div class='page-header'>
				<h2>Currency Rates Checker</h2>
				<p>To use our virtual checker you have to be logged in</p>
			</div>
			<div class="form-group">
				<label for="usr">Login:</label> <input name="userId" type="text"
					class="form-control" id="usr" />
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input name="password"
					type="password" class="form-control" id="pwd" />
			</div>
			<div>
				<input class='btn  btn-default' type="submit" value="Login" />
			</div>
			<div class="text-right">
				<span>Don't have an account yet? Oww... <a
					href="register.jsp">Register here</a></span>
			</div>

		</form>

	</div>



</body>
</html>
