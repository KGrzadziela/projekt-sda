<%@page import="service.NbpService"%>
<%@page import="java.util.List"%>
<%@page import="service.LoginService"%>
<%@page import="model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />

<title>Result Page</title>
</head>
<body>

	<div id="container">
		<center>
				<H2>Currency Rates Checker</H2>

			<br> <br>
			<%
				User user = (User) session.getAttribute("user");
				List<Currency> listCur;
				NbpService nbpService = new NbpService();
			%>
			<b>Welcome <%=user.getFirstName() + " " + user.getLastName()%> !
			</b> <br /> <br /> <b>Here you can easily check the newest currency
				rates, compare them and make basic calculations!</b><br /> <a
				href="logout.jsp">Logout</a>
			</p>
			<br>

			<!--<table>
				<thead>
					<tr>
						<th>User ID</th>
						<th>First Name</th>
						<th>Middle Name</th>
						<th>Last Name</th>
						<th>email</th>
					</tr>
				</thead>
				<tbody>
					<%LoginService loginService = new LoginService();
			List<User> list = loginService.getListOfUsers();
			for (User u : list) {%>
					<tr>
						<td><%=u.getUserId()%></td>
						<td><%=u.getFirstName()%></td>
						<td><%=u.getMiddleName()%></td>
						<td><%=u.getLastName()%></td>
						<td><%=u.getEmail()%></td>
					</tr>
					<%}%>
				
				<tbody>  
			</table> -->
		</center>

		<H2>Check NBP data:</H2>
		Please input dates<br /> <br />
		<form method="post" action="NbpService">
			Date 1: <br /> <input name="date1" title="Date1" value="" size="30"
				maxlength="50" placeholder="YYYY-MM-DD" /> <br /> <br /> <br />Date
			2: <br /> <input name="date2" title="Date2" value="" size="30"
				maxlength="50" placeholder="YYYY-MM-DD" /> <br /> <br /> <br />Currency
			Code: <br /> <select name="currencyCode">
				<option value="USD">USD</option>
				<option value="EUR">EUR</option>
				<option value="AUD">AUD</option>
				<option value="CAD">CAD</option>
				<option value="HUF">HUF</option>
				<option value="GBP">GBP</option>
				<option value="JPY">JPY</option>
			</select> <br /> <br /> <input style="width: 60px;" type="submit"
				value="Send" /> <br /> <br /> <br />
		</form>
		<H2>Result:</H2>

		<table>
			<thead>
				<tr>
					<th>Code</th>
					<th>Rate</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
				<%
					System.out.println(NbpService.date1full);

					if (NbpService.date1full != null) {
						listCur = nbpService.printCurrenciesFromServer(NbpService.dates);

						//else listCur = nbpService.callCurrencies();//nbpService.printCurrenciesFromServer("c080z160426");
						for (Currency c : listCur) {
				%>
				<tr>
					<td><%=c.getCode()%></td>
					<td><%=c.getRate()%></td>
					<td><%=c.getName()%></td>
				</tr>
				<%
					}
					}
				%>
			
			<tbody>
		</table>
		<%
			if (nbpService.getAverage() > 0) {
		%>
		<br>
		<table>
			<thead>
				<th>Average:</th>
			</thead>
			<tr>
				<td><%=nbpService.getAverage()%></td>
			</tr>
			</br>
			<%
				}
			%>
			<%
				if (nbpService.getStd() > 0) {
			%>
			<thead>
				<th>Standard deviation:</th>
			</thead>
			<tr>
				<td><%=nbpService.getStd()%></td>
			</tr>
			<%
				}
			%>
		</table>
		<br>
	</div>
	</center>
</body>
</html>
