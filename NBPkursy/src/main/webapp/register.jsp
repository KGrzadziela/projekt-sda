<!-- TO DO: obsluga wyjatkow (tudzież idiotoodpornosc), poniewaz obecnie niezaleznie od tego co wpiszemy w formularz, uzytkownik zostanie zarejestrowany -->
<html>
<head>

<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />
</head>

<body>

	<div class='container'>
		<form action="RegisterServlet" method="POST" class='well'>
			<div class='page-header'>
				<h2>Registration Form</h2>
			</div>
			<div class="form-group">
				<label for="usr">First name:</label> <input name="firstname"
					type="text" class="form-control" id="usr" maxlength="30" />
			</div>
			<div class="form-group">
				<label for="usr">Middle name:</label> <input name="middlename"
					type="text" class="form-control" id="usr" maxlength="30" />
			</div>
			<div class="form-group">
				<label for="usr">Last name:</label> <input name="lastname"
					type="text" class="form-control" id="usr" maxlength="30" />
			</div>
			<div class="form-group">
				<label for="usr">Email:</label> <input name="email" type="text"
					class="form-control" id="usr" maxlength="30" />
			</div>
			<div class="form-group">
				<label for="usr">Login:</label> <input name="userId" type="text"
					class="form-control" id="usr" />
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input name="password"
					type="password" class="form-control" id="pwd" />
			</div>
			<div>
				<input class='btn  btn-default' type="submit" value="Login" /> <input
					class='btn  btn-warning' type="reset" value="Reset">
			</div>
		</form>
	</div>
</body>
</html>