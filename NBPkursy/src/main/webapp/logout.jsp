
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />
<title>Goodbye Page</title>
</head>
<body>
	<%
		session.removeAttribute("userId");
		session.removeAttribute("password");
		session.invalidate();
	%>
	<div class='well'>
		<h1>You have been successfully logged out</h1>
		To login again <a href="login.jsp">click here</a>.
	</div>
</body>
</html>