package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MyDocument {

	private File inputFile;
	private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	private DocumentBuilder dBuilder;
	private MyDocument doc;
	private String date1form;
	private String date1full = null;
	private List<String> dates = new ArrayList<String>();
	private String date2form;
	private String date2full = null;
	private String currencyCode = "USD";

	public File getInputFile() {
		return inputFile;
	}

	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}

	public DocumentBuilderFactory getDbFactory() {
		return dbFactory;
	}

	public void setDbFactory(DocumentBuilderFactory dbFactory) {
		this.dbFactory = dbFactory;
	}

	public DocumentBuilder getdBuilder() {
		return dBuilder;
	}

	public void setdBuilder() throws ParserConfigurationException {
		this.dBuilder = dbFactory.newDocumentBuilder();

	}

	public MyDocument getDoc() {
		return doc;
	}

	public void setDoc(MyDocument doc) {
		this.doc = doc;
	}

	public String getDate1form() {
		return date1form;
	}

	public void setDate1form(HttpServletRequest request, String argument) {
		String output = request.getParameter(argument);
		output = output.substring(2).replace("-", "");
		this.date1form = output;
	}

	public String getDate1full() {
		return date1full;
	}

	public void setDate1full(String date1full) {
		this.date1full = date1full;
	}

	public List<String> getDates() {
		return dates;
	}

	public void setDates(List<String> dates) {
		this.dates = dates;
	}

	public String getDate2form() {
		return date2form;
	}

	public void setDate2form(HttpServletRequest request, String argument) {
		String output = request.getParameter(argument);
		output = output.substring(2).replace("-", "");
		this.date2form = output;
	}

	public String getDate2full() {
		return date2full;
	}

	public void setDate2full(String date2full) {
		this.date2full = date2full;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public MyDocument() {
	}

	public MyDocument(File inputFile, DocumentBuilderFactory dbFactory, DocumentBuilder dBuilder, MyDocument doc,
			String date1form, String date1full, List<String> dates, String date2form, String date2full,
			String currencyCode) {
		this.inputFile = inputFile;
		this.dbFactory = dbFactory;
		this.dBuilder = dBuilder;
		this.doc = doc;
		this.date1form = date1form;
		this.date1full = date1full;
		this.dates = dates;
		this.date2form = date2form;
		this.date2full = date2full;
		this.currencyCode = currencyCode;
	}

}
