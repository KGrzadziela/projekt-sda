package service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import model.Currency;

public class NbpService extends HttpServlet {

    File inputFile;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    Document doc;
    String date1form;
    public static String date1full = null;
    public static List<String> dates = new ArrayList<String>();
    public static double average = 0;
    public static double std = 0;
    String date2form;
    public static String date2full = null;
    public static String currencyCode = "USD";
   

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        date1form = request.getParameter("date1");
        date2form = request.getParameter("date2");
        this.currencyCode = request.getParameter("currencyCode");
        date1form = date1form.substring(2).replace("-", "");
        date2form = date2form.substring(2).replace("-", "");
        
        try {
            this.dbFactory = DocumentBuilderFactory.newInstance();
            this.dBuilder = dbFactory.newDocumentBuilder();
            StringBuilder txt = new StringBuilder();
            String str = "";
            int str2;
            URL url = new URL("http://www.nbp.pl/kursy/xml/dir.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            this.dates = new ArrayList<String>();
            while ((str = in.readLine()) != null ) {
                txt.append(str);
                str = str.replace("ď»ż", "");
                str2 =  Integer.parseInt(str.substring(5));
                if (str.contains("c") && str2 <= Integer.parseInt(date2form) && str2 >= Integer.parseInt(date1form)) {
                   this.date1full = str;
                   this.dates.add(str);
                }
            }
            in.close();
            //this.doc = dBuilder.parse("http://www.nbp.pl/kursy/xml/dir.txt");
            //this.doc.getDocumentElement().normalize();
            response.sendRedirect("home.jsp");
        } catch (Exception e) {

            e.printStackTrace();
        }

    }
    
    public List<Currency> callCurrencies() {
        List<String> x = new ArrayList<String>();
        List<Currency> z;
        x.add("c080z160426");
        z =  this.printCurrenciesFromServer(x);
        return z;
        
     }

    public List<Currency> printCurrenciesFromServer(List<String> filelist) {
        
        List<Currency> listCur = new ArrayList<Currency>();
        String filename = "";
        int i = 0;
        average = 0.0;
        std = 0.0;
        for( String item : filelist) {
            filename = item.replace("ď»ż", "");
            try {
                this.dbFactory = DocumentBuilderFactory.newInstance();
                this.dBuilder = dbFactory.newDocumentBuilder();
                this.doc = dBuilder.parse("http://www.nbp.pl/kursy/xml/" + filename + ".xml");
                this.doc.getDocumentElement().normalize();

                NodeList nList = this.doc.getElementsByTagName("pozycja");
                   
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String code = eElement.getElementsByTagName("kod_waluty").item(0).getChildNodes().item(0).getNodeValue();
                        String name = eElement.getElementsByTagName("nazwa_waluty").item(0).getChildNodes().item(0).getNodeValue();
                        StringBuilder input = new StringBuilder(eElement.getElementsByTagName("kurs_kupna").item(0).getChildNodes().item(0).getNodeValue());
                        input.setCharAt(1, '.');
                        Double rate = Double.parseDouble(input.toString());
                        Currency cur = new Currency(code, name, rate);
                        
                        if((cur.getCode()).contentEquals(this.currencyCode)){
                            i++;
                            listCur.add(cur);
                            average += rate;
                        } else {
                        }

                    }

                }

            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        average = average / i;
       
        for (Currency c : listCur) {
            std += (c.getRate() - average) * (c.getRate() - average);
        }
        std /= i;
        std = Math.sqrt(std);
        return listCur;

    }
    
    
    public double getAverage() {
        return average;
    }
    
    public double getStd() {
        return std;
    }

}
